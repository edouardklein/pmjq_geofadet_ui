(ns interf.page
  (:require
   [cljs.core.async :as async :refer [put!]]
   [reagent.core :as reagent]
   )
  )


(defn send-upload!
  "Receive an uploaded file and send it"
  [upload e uploaded-file dirname]
  (swap! upload assoc-in [dirname "state"] "proc") ; Upload by input
  (put! uploaded-file [e dirname]))

(defn log-text-zone
  "A text area that automatically scrolls down when changed."
  [logs]
  (reagent/create-class
   {:display-name "log-textarea"
    :component-did-mount
    (fn []
      (let [textarea (.getElementById js/document "Logs")]
        (.log js/console "Scrolling the logs")
        (aset textarea "scrollTop" (aget textarea "scrollHeight"))))
    :reagent-render
    (fn [logs]
      (.log js/console "Rendering the logs")
      [:div  "Logs :"
       [:div
        [:textarea#Logs {:rows 10 :cols 150 :readOnly true
                         :value (:text @logs)}]]])}))


(defn page
  "Web interface content"
  [upload queues logs errors result upload-chan]
  [:div
   [:h1 "Geofadet - Traitement des FADETs"]
   [:div
    ;; Upload buttons (name, waiting queue, button)
    (let [states (set (for [upl (vals @upload)](:state upl)))]
      [:div (doall (for [dirname (keys @upload)]
               (let [state (get-in @upload [dirname :state])
                     num (get @queues dirname)
                     typ ".xml"]
                 ^{:key dirname} [:div (str
                                        "Fichier d'entrée de type "
                                        typ
                                        " :"
                                        (if num (str " (File d'attente : " num ")"))
                                        "\t-> ")
                                  [:input  {:name dirname
                                            :id dirname
                                            :accept [typ]
                                            :type "file"
                                            :disabled (or (some? state) (not-every? nil? states))
                                            :on-change
                                            #(send-upload! upload % upload-chan dirname)}]])))
   (doall (for [dirname (keys @upload)]
            ;; Upload statuses
            (let [typ "XML"
                  state (get-in @upload [dirname :state])
                  status (fn [s] ^{:key (str dirname "status")}
                           [:p (str "Statut (type " typ ") : " s)])]
              (cond (and (= state nil) (not-every? nil? states)) ""
                    (= state nil) (status "En attente...")
                    (= state "proc") (status "Envoi...")
                    :else (status "Envoyé.")))))])]
   [:button {:type "button" :on-click #(swap! logs update-in [:show] not)}
    "Afficher/cacher les logs (indiquent le traitement du fichier)"]
   (if (:show @logs)
     [log-text-zone logs])
   [:p (if @errors
     (doall (for [err (vals @errors)]
              ;; Show error content
              ^{:key (:command err)} [:div
                                      [:p [:font {:color "red"}
                                           (str "Erreur lors du traitement de " (:command err) ": ")]
                                       [:button
                                        {:type "button"
                                         :on-click
                                         #(swap! errors update-in
                                                 [(:command err) :show] not)}
                                        "Afficher"]]
                                      (if (:show err)
                                        [:textarea {:readOnly true :rows 15
                                                    :cols 100
                                                    :value (or
                                                            (:message err)
                                                            "")}])])))]
   (doall
    ;; Output hyperlinks
    (for [file @result]
      ^{:key file} [:div 
                    (let [
                          folder  (peek (clojure.string/split file #"/"))
                          name (clojure.string/join
                                ""
                                (drop-last (get (clojure.string/split folder #"-") 0)))
                          dlname (case name
                                   "exported" "Cellules localisées"
                                   "merged" "Fichier complet (localisations présentes + manquantes)"
                                   "to_requis" "Localisations manquantes"
                                   "extracted_logs" "Fichier de log - IMPORTANT"
                                   "to_geocode" "")
                          ]
                      [:a {:href file :target "_blank" :id name :class "download-link"}(if-not (empty? dlname)(str  "Télécharger - " dlname))])]))
[:p
 (let [states (set (for [upl (vals @upload)](:state upl)))]
   (if (not-empty @errors)
     ""
     (if (or (empty? states) (every? nil? states))
       "En attente de fichiers."
       (if (not (contains? states "proc"))
         (if (= (count @result) 5) "Terminé." [:div "Attente des résultats..." [:p [:img {:src "loading.gif"}]]])
         "Envoi..."))))
]
[:p {:id "reload-info"} "(F5 pour recharger/réinitialiser la page)"]])

